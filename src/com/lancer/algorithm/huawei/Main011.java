package com.lancer.algorithm.huawei;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Main011 {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = "";
        while((line=br.readLine())!=null){
            int N = Integer.parseInt(line);
            // 负数数量
            int negativeCount=0;
            // 正数数量
            int positiveCount=0;
            // 正数之和
            int positiveSum = 0;
            String[] strs = br.readLine().split(" ");
            for(int i=0;i<strs.length;++i){
                int temp = Integer.parseInt(strs[i]);
                if(temp > 0){
                    ++positiveCount;
                    positiveSum += temp;
                }else if(temp < 0){
                    ++negativeCount;
                }
            }
            System.out.println(negativeCount+" "+Math.round(positiveSum * 10.0/positiveCount)/10.0);
        }
    }
}