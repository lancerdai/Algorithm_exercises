package com.lancer.algorithm.huawei;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 题目描述
 * 自守数是指一个数的平方的尾数等于该数自身的自然数。例如：25^2 = 625，76^2 = 5776，9376^2 = 87909376。请求出n以内的自守数的个数
 *
 *
 * 接口说明
 * 功能: 求出n以内的自守数的个数
 *
 *
 * 输入参数：
 * int n
 *
 * 返回值：
 * n以内自守数的数量。
 *
 * 输入描述:
 * int型整数
 *
 * 输出描述:
 * n以内自守数的数量。
 *
 * 示例1
 * 输入
 * 2000
 * 输出
 * 8
 */
public class Main010 {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input;
        while ((input = br.readLine()) != null){
            int num = Integer.parseInt(input);
            System.out.println(CalcAutomorphicNumbers(num));
        }
    }

    public static int CalcAutomorphicNumbers( int n)
    {
        // 0是第一个自守数，从1开始循环，所以直接写1
        int count = 1;
        for(int i = 1 ; i< n; i++){
            // 首先知道i是几位数
            int k = 1;
            int tmp = i;
            while (tmp != 0){
                tmp = tmp / 10;
                k = k * 10;
            }
            if( ((i * i ) % k )== i ){
                count++;
            }
        }
        return count;
    }

}
