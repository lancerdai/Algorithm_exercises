package com.lancer.algorithm.huawei;

import java.util.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;

/**
 *题目描述
 * 考试题目和要点：
 * 1、中文大写金额数字前应标明“人民币”字样。中文大写金额数字应用壹、贰、叁、肆、伍、陆、柒、捌、玖、拾、佰、仟、万、亿、元、角、分、零、整等字样填写。（30分）
 * 2、中文大写金额数字到“元”为止的，在“元”之后，应写“整字，如￥ 532.00应写成“人民币伍佰叁拾贰元整”。在”角“和”分“后面不写”整字。（30分）
 * 3、阿拉伯数字中间有“0”时，中文大写要写“零”字，阿拉伯数字中间连续有几个“0”时，中文大写金额中间只写一个“零”字，如￥6007.14，应写成“人民币陆仟零柒元壹角肆分“。（
 *
 * 输入描述:
 * 输入一个double数
 *
 * 输出描述:
 * 输出人民币格式
 *
 * 示例1
 * 输入
 * 151121.15
 * 输出
 * 人民币拾伍万壹仟壹佰贰拾壹元壹角伍分
 *
 */
public class Main013 {

    private static String charAt(String str, int index){
        if( str == null || index >= str.length() || index < 0){
            return "";
        }
        return str.substring(index, index+1);
    }
    public static void main(String[] args) throws Exception{
        Map<String,String> digitMap = new HashMap<String,String>();
        digitMap.put("0","零");
        digitMap.put("1","壹");
        digitMap.put("2","贰");
        digitMap.put("3","叁");
        digitMap.put("4","肆");
        digitMap.put("5","伍");
        digitMap.put("6","陆");
        digitMap.put("7","柒");
        digitMap.put("8","捌");
        digitMap.put("9","玖");
        // 单位
        String[] unitStr = new String[]{"拾","佰","仟","万","亿"};
        // 钱的单位
        String[] moneyUnit = new String[]{"分","角","元","整"};
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = "";
        while((line=br.readLine())!=null){
            String[] inputArr = line.split("\\.");
            StringBuilder bd = new StringBuilder("人民币");
            // 小数点前后分成两部分
            String part1 = inputArr[0];
            String part2 = inputArr[1];
            if(!part1.equals("0")){
                for(int i =0;i < part1.length();i++){
                    // 当前数字后面还有多少个数字
                    int unitLen = part1.length()-i-1;
                    // 数字转义
                    String number = (String)digitMap.get(charAt(part1, i));
                    String unit = "";
                    if( unitLen == 0){// 最后一个数字，后面跟“元”
                        unit = moneyUnit[2];
                    } else if(unitLen == 1){// 倒数第二个数字，后面跟“拾”
                        unit = unitStr[0];
                    } else if(unitLen == 2){// 倒数第三个数字，后面跟“佰”
                        unit = unitStr[1];
                    } else if(unitLen == 3){// 倒数第四个数字，后面跟“仟”
                        unit = unitStr[2];
                    } else if(unitLen >= 4 && unitLen < 8){// 倒数第5-8个数字，后面跟“万”
                        unit = unitStr[3];
                    } else if(unitLen >= 8){// 倒数第9个数字和以上，后面跟“亿”
                        unit = unitStr[4];
                    }
                    if(part1.length() == 2 && i == 0){// 如果是两位数，并且i是0，直接输出单位
                        bd.append(unit);
                    } else {// 输出数字+单位
                        bd.append(number+unit);
                    }
                }
            }
            // 小数点后
            if(!part2.equals("00")){
                for(int j =0;j < part2.length();j++){
                    // 数字转义
                    String number2 = (String)digitMap.get(charAt(part2, j));
                    // 如果是0，不输出
                    if(number2.equals("零")){
                        continue;
                    }
                    String unit2 = "";
                    if( j == 0 ){// 第一个小数，单位是“角”
                        unit2 = moneyUnit[1];
                    } else if(j == 1){// 第二个小数，单位是“分”
                        unit2 = moneyUnit[0];
                    }
                    // 输出数字+单位
                    bd.append(number2+unit2);
                }
            } else{// 如果没有小数，输出“整”
                bd.append("整");
            }
            System.out.println(bd);
        }
    }

}
