package com.lancer.algorithm.huawei;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *题目描述
 * 如果统计的个数相同，则按照ASCII码由小到大排序输出 。如果有其他字符，则对这些字符不用进行统计。
 *
 * 实现以下接口：
 * 输入一个字符串，对字符中的各个英文字符，数字，空格进行统计（可反复调用）
 * 按照统计个数由多到少输出统计结果，如果统计的个数相同，则按照ASII码由小到大排序输出
 * 清空目前的统计结果，重新统计
 * 调用者会保证：
 * 输入的字符串以‘\0’结尾。
 *
 *
 * 输入描述:
 * 输入一串字符。
 *
 * 输出描述:
 * 对字符中的
 * 各个英文字符（大小写分开统计），数字，空格进行统计，并按照统计个数由多到少输出,如果统计的个数相同，则按照ASII码由小到大排序输出 。如果有其他字符，则对这些字符不用进行统计。
 *
 * 示例1
 * 输入
 * aadddccddc
 * 输出
 * dca
 */
public class Main007 {


    /**
     *
     * @param args
     */
    public static void main(String[] args) throws IOException {

        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        String line;
        while ( (line = bufferedReader.readLine()) != null){
            line  = bufferedReader.readLine();
            System.out.println(func(line));
        }
    }

    private static String func(String line) {
        char[] chars = line.toCharArray();
        // ASCII码总共128个，用这个数组记录每个字符出现的个数，i就是ASCII序号，value就是出现次数
        // 空格也在ASCII码中，不需要单独考虑
        int[] nums = new int[130];
        // 计算每个字符出现的次数
        for(char c : chars){
            nums[(int)c] ++;
        }

        // 计算出现次数最多的次数
        int max = 0;
        for(int i : nums){
            if(i > max){
                max = i;
            }
        }

        // 拼接结果
        StringBuilder sb = new StringBuilder();

        // 出现次数从大到小循环，保证出现次数最多的在前面
        while (max > 0){
            // ASCII码数值从小到大循环，保证次数相同时，数值小的在前面
            for(int i = 0;i < nums.length; i++){
                if(nums[i] == max){
                    sb.append((char)i);
                }
            }
            max--;
        }
        return sb.toString();

    }


}
