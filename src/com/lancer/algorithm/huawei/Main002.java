package com.lancer.algorithm.huawei;

import java.util.Scanner;

/**
 * 题目描述
 * •计算一个数字的立方根，不使用库函数
 *
 * 详细描述：
 * •接口说明
 * 原型：
 *
 * public static double getCubeRoot(double input)
 *
 * 输入:double 待求解参数
 *
 * 返回值:double  输入参数的立方根，保留一位小数
 *
 *
 * 输入描述:
 * 待求解参数 double类型
 *
 * 输出描述:
 * 输入参数的立方根 也是double类型
 *
 * 示例1
 * 输入
 * 216
 * 输出
 * 6.0
 */
public class Main002 {

    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        if(scanner.hasNext()){
            double input = scanner.nextDouble();

            System.out.println(String.format("%.1f", getCubeRoot(input)));
        }
    }

    /**
     * 求立方根
     * @param input
     * @return
     */
    private static double getCubeRoot(double input){
        // 左指针
        double left = input > 0 ? 0 : input;
        // 右指针
        double right = input > 0 ? input : 0;
        while(true){
            if((int)(left * 100)  == (int) (right * 100) ){
                // 如果保留两位小数后，左右数字相等，则不需要继续计算直接返回
                // 如果一位小数就返回的话，会导致四舍五入结果有差异，这里必须是两位
                return left;
            }
            // 二分
            double half = (left + right) / 2;
            // 二分的立方
            double trible = half * half * half;
            if(trible < input){ // 如果半数的立方，比input小，取右侧
                left  = half;
            }else if( trible > input){ // 如果半数的立方，比input大，取左侧
                right = half;
            }else{ // 如果不大不小，返回
                return half;
            }
        }
    }
}
