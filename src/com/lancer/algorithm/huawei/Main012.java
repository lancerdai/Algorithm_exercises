package com.lancer.algorithm.huawei;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * 题目描述
 * 将一个字符中所有出现的数字前后加上符号“*”，其他字符保持不变
 * public static String MarkNum(String pInStr)
 * {
 *
 * return null;
 * }
 * 注意：输入数据可能有多行
 * 输入描述:
 * 输入一个字符串
 *
 * 输出描述:
 * 字符中所有出现的数字前后加上符号“*”，其他字符保持不变
 *
 * 示例1
 * 输入
 * Jkdi234klowe90a3
 * 输出
 * Jkdi*234*klowe*90*a*3*
 */
public class Main012 {

    public static void main(String[] args) throws IOException {
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));
        String str;
        while((str=br.readLine())!=null){
            System.out.println(MarkNum(str));
        }
    }

    public static String MarkNum(String str) {
        StringBuffer sb=new StringBuffer();
        // 标记上一个字符是否是数字
        boolean flag=false;
        for(int i=0;i<str.length();i++){
            if(str.charAt(i)-'0'>=0 && str.charAt(i)-'0'<=9){// 如果是数字
                if(flag){// 如果上一个是数字，直接输出这个数字
                    sb.append(str.charAt(i));
                }else{// 如果上一个不是数字，输出开始的*，再输出数字
                    flag=true;
                    sb.append("*").append(str.charAt(i));
                }
                if(i==str.length()-1){// 如果是最后一个字符，输出结束的*
                    sb.append("*");
                }
            }else{// 如果不是数字
                if(flag){// 如果上一个是数字，输出结束的*，然后输出字符
                    flag=false;
                    sb.append("*").append(str.charAt(i));
                }else{// 如果上一个也不是数字，直接输出这个字符
                    sb.append(str.charAt(i));
                }
            }
        }
        return sb.toString();
    }

}
