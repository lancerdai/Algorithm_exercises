package com.lancer.algorithm.huawei;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *连续输入字符串(输出次数为N,字符串长度小于100)，请按长度为8拆分每个字符串后输出到新的字符串数组，
 *
 * 长度不是8整数倍的字符串请在后面补数字0，空字符串不处理。
 *
 * 首先输入一个整数，为要输入的字符串个数。
 *
 * 例如：
 *
 * 输入：2
 *
 * abc
 *
 * 12345789
 *
 * 输出：abc00000
 *
 * 12345678
 *
 * 90000000
 *
 *输入描述:
 * 首先输入数字n，表示要输入多少个字符串。连续输入字符串(输出次数为N,字符串长度小于100)。
 *
 * 输出描述:
 * 按长度为8拆分每个字符串后输出到新的字符串数组，长度不是8整数倍的字符串请在后面补数字0，空字符串不处理。
 *
 * 示例1
 * 输入
 * 复制
 * 2
 * abc
 * 123456789
 * 输出
 * 复制
 * abc00000
 * 12345678
 * 90000000
 */
public class Main005 {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input;
        while((input = br.readLine())!=null){
            int count = Integer.parseInt(input);
            StringBuilder result = new StringBuilder();
            for(int i = 0;i<count;i++){
                input = br.readLine();
                int start = 0;
                int end = 8;
                int length = input.length()/8;

                if(input.length()%8>0){
                    ++length;
                }
                for(int j=0;j<length;j++){
                    end = end>input.length()?input.length():end;
                    String current = input.substring(start,end);
                    result.append(current);
                    if(current.length()<8){
                        for(int k=0;k<8-current.length();k++){
                            result.append("0");
                        }
                    }
                    result.append("\n");
                    start+=8;
                    end+=8;
                }
            }
            System.out.println(result.toString().trim());
        }
    }
}
