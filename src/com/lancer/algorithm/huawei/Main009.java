package com.lancer.algorithm.huawei;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 *题目描述
 * 功能:等差数列 2，5，8，11，14。。。。
 *
 * 输入:正整数N >0
 * 输出:求等差数列前N项和
 *
 * 返回:转换成功返回 0 ,非法输入与异常返回-1
 * 本题为多组输入，请使用while(cin>>)等形式读取数据
 * 输入描述:
 * 输入一个正整数。
 * 输出描述:
 * 输出一个相加后的整数。
 *
 * 示例1
 * 输入
 * 2
 * 输出
 * 7
 *
 *
 *
 */
public class Main009 {

    /**
     * 高中数学知识，等差数列前n项的和，= Na1 + +n(n-1)d/2
     */

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input;
        while ((input = br.readLine()) != null){
            int n = Integer.parseInt(input);

            System.out.println( ( 2 * n + ((3 * n * (n -1))) / 2) );


        }




    }


}
