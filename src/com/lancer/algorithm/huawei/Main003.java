package com.lancer.algorithm.huawei;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 题目描述
 * 将一个字符串str的内容颠倒过来，并输出。str的长度不超过100个字符。 如：输入“I am a student”，输出“tneduts a ma I”。
 *
 * 输入参数：
 * inputString：输入的字符串
 * 返回值：
 * 输出转换好的逆序字符串
 *
 * 输入描述:
 * 输入一个字符串，可以有空格
 *
 * 输出描述:
 * 输出逆序的字符串
 *
 * 示例1
 * 输入
 * I am a student
 * 输出
 * tneduts a ma I
 */

public class Main003 {

    public static void main(String[] args) throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        String str = bf.readLine();
        System.out.println(perversionStr(str));
    }

    /**
     * 倒序字符串
     * @param input
     * @return
     */
    public static String perversionStr(String input){
        StringBuffer sb = new StringBuffer();
        char[] chars = input.toCharArray();
        for(int i = chars.length -1; i >= 0; i--){
            sb.append(chars[i]);
        }
        return sb.toString();
    }
}
