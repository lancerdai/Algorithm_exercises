package com.lancer.algorithm.huawei;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * 题目描述
 * 样例输出
 *
 * 输出123058789，函数返回值9
 *
 * 输出54761，函数返回值5
 *
 *
 *
 * 接口说明
 *
 * 函数原型：
 *
 *    unsignedint Continumax(char** pOutputstr,  char* intputstr)
 *
 * 输入参数：
 *    char* intputstr  输入字符串；
 *
 * 输出参数：
 *    char** pOutputstr: 连续最长的数字串，如果连续最长的数字串的长度为0，应该返回空字符串；如果输入字符串是空，也应该返回空字符串；
 *
 * 返回值：
 *   连续最长的数字串的长度
 *
 *
 * 输入描述:
 * 输入一个字符串。
 *
 * 输出描述:
 * 输出字符串中最长的数字字符串和它的长度。如果有相同长度的串，则要一块儿输出，但是长度还是一串的长度
 *
 * 示例1
 * 输入
 * abcd12345ed125ss123058789
 * 输出
 * 123058789,9
 *
 *
 */
public class Main016 {

    public static void main(String[] args)throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        String str;
        while((str=bf.readLine()) != null){
            // 最大长度
            int max = 0;
            // 最长字符串
            String res = "";

            int temp = 0;
            // 正在遍历的数字长度
            int cnt = 0;
            //  正在遍历的数字字符串
            String tstr = "";
            for(int i=0; i<str.length(); i++){
                char c = str.charAt(i); //从0开始
                if(c >= '0' && c <= '9'){
                    // 当前是不是数字
                    if(temp == 0){
                        temp = 1;
                    }
                    tstr += c+"";
                    cnt++;
                    continue; //直接continue
                }
                else{  //遇到第一个非数字字符重置相关的中间变量
                    if(cnt >= max){// 结算
                        if(cnt > max){
                            max = cnt;
                            res = tstr;
                        }
                        else{
                            res += tstr;
                        }
                    }
                    // 最后一位不是数字
                    temp = 0;
                    // 数字长度置0
                    cnt = 0;
                    // 数字字符串置空
                    tstr = "";
                }
            }
            // 如果最后一位是数字
            if(temp == 1){
                if(cnt >= max){
                    if(cnt > max){
                        max = cnt;
                        res = tstr;
                    }
                    else{
                        res += tstr;
                    }
                }
            }
            System.out.println(res + "," + max);
        }
        bf.close();
    }
}
