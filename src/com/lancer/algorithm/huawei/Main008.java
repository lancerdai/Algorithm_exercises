package com.lancer.algorithm.huawei;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 题目描述
 * 输入整型数组和排序标识，对其元素按照升序或降序进行排序（一组测试用例可能会有多组数据）
 *
 * 接口说明
 * 原型：
 * void sortIntegerArray(Integer[] pIntegerArray, int iSortFlag);
 * 输入参数：
 * Integer[] pIntegerArray：整型数组
 * int  iSortFlag：排序标识：0表示按升序，1表示按降序
 *
 * 输出参数：
 * 无
 * 返回值：
 * void
 *
 * 输入描述:
 * 1、输入需要输入的整型数个数
 *
 * 输出描述:
 * 输出排好序的数字
 *
 * 示例1
 * 输入
 * 8
 * 1 2 4 9 3 55 64 25
 * 0
 * 输出
 * 1 2 3 4 9 25 55 64
 *
 *
 */
public class Main008 {

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String input;

        while ((input = br.readLine()) != null){
            // 数字个数
            int count = Integer.parseInt(input);
            // 获取数组
            String arrayStr = br.readLine();
            String[] stringArray = arrayStr.split(" ");
            Integer[] array = new Integer[stringArray.length];
            for(int i = 0; i < stringArray.length; i++){
                array[i] = Integer.parseInt(stringArray[i]);
            }

            // 升序or降序
            int flag = Integer.parseInt(br.readLine());

            sortIntegerArray(array,flag);
            printArray(array);

        }
    }

    /**
     * 冒泡排序
     * @param pIntegerArray
     * @param iSortFlag
     */
    private static void sortIntegerArray(Integer[] pIntegerArray, int iSortFlag){
        boolean flag = true;
        while (flag){
            flag = false;
            for(int i = 0; i < pIntegerArray.length -1 ; i++){
                if(iSortFlag == 1 && pIntegerArray[i] < pIntegerArray[i+1]
                        || iSortFlag == 0 && pIntegerArray[i] > pIntegerArray[i+1]) {
                    //交换位置
                    Integer tmp = pIntegerArray[i];
                    pIntegerArray[i] = pIntegerArray[i+1];
                    pIntegerArray[i+1] = tmp;
                    flag = true;

                }
            }
        }
    }


    /**
     * 打印数组
     * @param arr
     */
    private static void printArray(Integer[] arr){
        StringBuilder sb = new StringBuilder();
        for(Integer i : arr){
           sb.append(i + " ");
        }
        System.out.println(sb.toString().trim());
    }


}
