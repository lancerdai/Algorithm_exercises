package com.lancer.algorithm.huawei;

import java.util.Scanner;

/**
 * 正整数A和正整数B 的最小公倍数是指 能被A和B整除的最小的正整数值，设计一个算法，求输入A和B的最小公倍数。
 *
 * 输入描述:
 * 输入两个正整数A和B。
 *
 * 输出描述:
 * 输出A和B的最小公倍数。
 *
 * 示例1
 * 输入
 * 5 7
 * 输出
 * 35
 *
 */
public class Main001 {


    /**
     * 算法解析：
     *  两个数字相乘，再除以最大公约数，得到的数字就是最小公倍数
     *  所以先用辗转相除法，求出最大公约数，就可以得到结果
     */


    /**
     *
     * @param args
     */
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        if(scanner.hasNext()){
            int x = scanner.nextInt();
            int y = scanner.nextInt();
            System.out.println(x*y / getResult2(x, y));
        }
    }

    /**
     * 辗转相除法求最大公约数，while循环法
     * @param x
     * @param y
     * @return
     */
    private static int getResult(int x,int y){
        // 保证x < y
        if(x > y){
            int tmp = y;
            y = x;
            x = tmp;
        }
        // k用来存储余数，防止多次初始化，所以在外面定义
        int k;
        while(x != 0){// 小的数字不为0时，保持循环
            k = y % x; // 求余数
            y = x;  // 小数变为大数
            x = k;  // 余数变为小数
        }
        return y; // 最终较大的数字为最终结果
    }


    /**
     * 辗转相除法求最大公约数，递归法
     * @param x
     * @param y
     * @return
     */
    private static int getResult2(int x,int y){
        // 保证x < y
        if(x > y){
            int tmp = y;
            y = x;
            x = tmp;
        }

        if(x == 0){ // 小数为0，计算结束跳出递归，最终的大数，就是最大公约数
            return y;
        }else{
            // 小数变大数，余数变小数，递归调用当前方法，求出最大公约数
            return getResult2(y % x,x);
        }
    }
}
